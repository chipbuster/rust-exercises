use std::assert;
use std::convert::TryInto;
use std::ptr::NonNull;

// This is so dumb yo.
pub fn segfault() {
    println!("Segmentation fault.");
    std::process::exit(1);
}

/// A FreeList is a doubly-linked list of FreeNodes. This is the structure used
/// to track free data for the malloc implementation. It needs to be paired with
/// a buffer to be useful.
pub struct FreeList {
    head: *mut FreeNode,
    tail: *mut FreeNode,
    // Might have issues with need a marker made of PhantomData later?
}

/// A FreeNode represents a single contiguous free piece of memory. It is denoted
/// by a pair of pointers, [lo,hi) representing the lowest valid and lowest invalid
/// addresses of the contiguous memory range. (i.e. half-open interval)
/// Note that the lo_ptr and hi_ptr in this struct should *never* be directly
/// dereferenced by this code, as they merely denote an available memory range.
#[derive(PartialEq, Debug, Clone)]
pub struct FreeNode {
    next: Option<NonNull<FreeNode>>,
    prev: Option<NonNull<FreeNode>>,
    lo_ptr: *const u8, // Denote the extent of the free space with two pointers
    hi_ptr: *const u8,
    in_use: bool,
}

enum NodeNeighbor {
    Prev,
    Ident,
    Next,
}

pub struct FLIter<'a> {
    next: Option<&'a FreeNode>,
}

pub struct FLRevIter<'a> {
    next: Option<&'a FreeNode>,
}

pub struct FLIterMut<'a> {
    next: Option<&'a mut FreeNode>,
}

pub struct FLRevIterMut<'a> {
    next: Option<&'a mut FreeNode>,
}

// Need to implement: deletion + insertion of node, search, and node splitting.
// Optional but useful: List iterators (remember that iterators must start high
// and go low)

impl FreeNode {
    /// Create a new FreeNode with given bounds, and floating in space (no prev/next)
    fn new(lo: *const u8, hi: *const u8) -> Self {
        FreeNode {
            next: None,
            prev: None,
            lo_ptr: lo,
            hi_ptr: hi,
            in_use: false,
        }
    }

    pub fn num_bytes(&self) -> usize {
        self.hi_ptr as usize - self.lo_ptr as usize
    }

    /// Given an address within the node's boundaries, split the node into two
    /// new nodes at that address.
    /// Panics: will panic if the split address is outside the node boundaries
    fn split_node(&self, split_at: *const u8) -> (Self, Self) {
        if split_at <= self.lo_ptr || split_at >= self.hi_ptr {
            panic!("Requested memory node to be split outside of its bounds");
        }

        let mut free_node = FreeNode::new(self.lo_ptr, split_at);
        let mut used_node = FreeNode::new(split_at, self.hi_ptr);

        // FreeNode::new doesn't quite do everything we need, we need to apply
        // some fixups afterwards. Link the nodes together.
        used_node.in_use = true;
        free_node.next = &mut used_node;
        used_node.prev = &mut free_node;

        (free_node, used_node)
    }

    /// Attempt to fuse two nodes, returning a single node which is the fusion
    /// of both.
    /// Panics: if the nodes do not represent adjacent memory blocks
    unsafe fn coalesce_nodes<'a>(lo_nd: &'a mut FreeNode, hi_nd: &FreeNode) -> &'a mut Self {
        assert!(
            lo_nd.hi_ptr == hi_nd.lo_ptr,
            "Tried to coalesce nodes that are not adjacent."
        );
        assert!(
            !lo_nd.in_use && !hi_nd.in_use,
            "Tried to coalesce nodes which are in use."
        );

        // Coalesce nodes by mutating lo_nd to encompass both ranges
        lo_nd.hi_ptr = hi_nd.hi_ptr;
        lo_nd.next = hi_nd.next;
        lo_nd
    }

    fn debug_print(&self) {
        print!("{:?} ", self as *const Self);
        print!("{:?}", self);
    }
}

/// Note: the FreeList should never be empty.
impl FreeList {
    /// Create a new FreeList corresponding to a particular heap, with no memory
    /// allocated
    pub fn new(begin: *const u8, size: usize) -> Self {
        let end: *const u8 = unsafe { begin.offset(size.try_into().unwrap()) };
        let node = Box::new(FreeNode::new(begin, end));
        let nptr = Box::into_raw(node);

        Self {
            head: nptr,
            tail: nptr,
        }
    }

    pub fn debug_print(&self) {
        print!("LL: ");
        for node in self.iter() {
            node.debug_print();
            print!(" ==> ");
        }
        println!(" #### ");
    }

    /// Insert the given node at the given location (between the two given nodes)
    fn insert_node(
        &mut self,
        prev: *mut FreeNode,
        next: *mut FreeNode,
        node: FreeNode,
    ) -> *mut FreeNode {
        let bnode = Box::new(node);
        let nptr = Box::into_raw(bnode);

        if prev.is_null() && next.is_null() {
            panic!("Requested insert into an empty list");
        }

        // Insert at beginning of list
        if prev.is_null() {
            self.head = nptr;
            unsafe {
                (*nptr).next = next;
                (*(*nptr).next).prev = nptr;
            }
            return unsafe { nptr };
        }

        // Insert at end of list
        if next.is_null() {
            self.tail = nptr;
            unsafe {
                (*nptr).prev = prev;
                (*(*nptr).prev).next = nptr;
            };
            return unsafe { nptr };
        }

        // Insert between two nodes
        unsafe {
            (*nptr).next = next;
            (*nptr).prev = prev;
            (*(*nptr).next).prev = nptr;
            (*(*nptr).prev).next = nptr;
        }

        unsafe { nptr }
    }

    /// Drops a node from the FreeList by unlinking it and then converting it
    /// to a Box (which drops at the end of scope)
    fn drop_node(&mut self, node: *mut FreeNode) {
        let node = unsafe { Box::from_raw(node) };
        let pnode = node.prev;
        let nnode = node.next;

        // We need to update previous/next nodes. While the list must always have
        // at least one node, the node could be on the end of the list, so we
        // still have to check for NULL-ness

        if pnode.is_null() {
            self.head = nnode;
        } else {
            unsafe {
                (*pnode).next = nnode;
            }
        }

        if nnode.is_null() {
            self.tail = pnode;
        } else {
            unsafe {
                (*nnode).prev = pnode;
            }
        }
    }

    /// Try to coalesce the node with the given NodeNeighbor. Return a reference
    /// to the node if no coalescing possible, or the coalesced node if it works
    fn try_coalesce_nodes(&mut self, node: &mut FreeNode, which: NodeNeighbor) -> *mut FreeNode {
        match which {
            NodeNeighbor::Prev => {
                if node.prev.is_null() {
                    return node as *mut FreeNode;
                }
                let prevnode = unsafe { &mut *node.prev };
                if prevnode.in_use {
                    return node as *mut FreeNode;
                }
                let coalesced = unsafe { FreeNode::coalesce_nodes(prevnode, node) };
                coalesced as *mut FreeNode
            }
            // Kind of a dumb case
            NodeNeighbor::Ident => node,
            NodeNeighbor::Next => {
                if node.next.is_null() {
                    return node as *mut FreeNode;
                }
                let nextnode = unsafe { &mut *node.next };
                if nextnode.in_use {
                    return node as *mut FreeNode;
                }
                let coalesced = unsafe { FreeNode::coalesce_nodes(node, nextnode) };
                coalesced as *mut FreeNode
            }
        }
    }

    /// Allocate memory from a node by splitting it, then linking its free
    /// half back into the linked list. The user is responsible for ensuring
    /// that the node has enough space--will panic otherwise.
    fn allocate_from_node(&mut self, node: *mut FreeNode, sz: usize) -> *const u8 {
        assert!(
            unsafe { (*node).num_bytes() >= sz },
            "Requested alloc from too small of a node"
        );
        assert!(sz > 0, "Requested allocation of zero or negative size");

        let split_addr = unsafe { (*node).hi_ptr.offset(-(sz as isize)) };
        let (free, used) = unsafe { (*node).split_node(split_addr) };

        assert!(
            free.hi_ptr == used.lo_ptr,
            "Free-used node pointer mismatch when allocating."
        );

        let ret_ptr = used.lo_ptr;

        // Current list layout: prev, node, next
        // Insert: prev, (insert free) node, next
        let prev = unsafe { (*node).prev };

        self.insert_node(prev, node, free);

        // Current list layout: prev, free, node, next
        // Insert: prev, free, (insert used), node, next
        let free = unsafe { (*node).prev };
        self.insert_node(free, node, used);

        // Current list layout: prev, free, used, node, next
        self.drop_node(node);

        ret_ptr
    }

    // A forward iterator on the function
    fn iter(&self) -> FLIter {
        FLIter {
            next: Some(unsafe { &*self.head }),
        }
    }

    fn rev_iter(&self) -> FLRevIter {
        FLRevIter {
            next: Some(unsafe { &*self.tail }),
        }
    }

    fn iter_mut(&self) -> FLIterMut {
        FLIterMut {
            next: Some(unsafe { &mut *self.head }),
        }
    }

    fn rev_iter_mut(&self) -> FLRevIterMut {
        FLRevIterMut {
            next: Some(unsafe { &mut *self.tail }),
        }
    }
}

impl FreeList {
    /// Attempt to allocate first-fit from the freelist.
    /// A return value of None indicates that the requested space was not available
    pub fn allocate_first_fit(&mut self, sz: usize) -> Option<*const u8> {
        let mut target: Option<*mut FreeNode> = None;
        for node in self.rev_iter_mut() {
            if !node.in_use && node.num_bytes() >= sz {
                target = Some(node as *mut FreeNode);
                break;
            }
        }
        target?;

        Some(self.allocate_from_node(target.unwrap(), sz))
    }
    pub fn allocate_best_fit(&mut self, sz: usize) -> Option<*const u8> {
        let mut target: Option<*mut FreeNode> = None;
        let mut slack = usize::max_value();
        for node in self.rev_iter_mut() {
            let nbytes = node.num_bytes();
            if !node.in_use && nbytes >= sz && nbytes - sz < slack {
                target = Some(node as *mut FreeNode);
                slack = nbytes - sz;
            }
        }
        target?;

        Some(self.allocate_from_node(target.unwrap(), sz))
    }

    pub fn free(&mut self, ptr: *mut u8) {
        let mut target: Option<*mut FreeNode> = None;
        for node in self.rev_iter_mut() {
            if node.lo_ptr == ptr {
                target = Some(node);
            }
        }
        if target.is_none() {
            segfault();
        }
        let mut target = unsafe { &mut *target.unwrap() };
        target.in_use = false;

        unsafe {
            target = &mut *self.try_coalesce_nodes(target, NodeNeighbor::Prev);
            target = &mut *self.try_coalesce_nodes(target, NodeNeighbor::Next);
        }
    }

    pub fn defragment(&mut self) {
        unsafe {
            // Store information about the list
            let end = (*self.tail).hi_ptr;
            let begin = (*self.head).lo_ptr;
            let newnode = Box::new(FreeNode {
                next: std::ptr::null_mut(),
                prev: std::ptr::null_mut(),
                lo_ptr: begin,
                hi_ptr: end,
                in_use: false,
            });

            // Store nodes that are in-use
            let mut vec: Vec<usize> = std::vec::Vec::new();
            for node in self.iter_mut() {
                if node.in_use {
                    vec.push(node.num_bytes());
                }
                // Drop the nodes as we go
                Box::from_raw(node as *mut FreeNode);
            }

            // Create a new node by consuming the box
            let nn = Box::into_raw(newnode);
            self.head = nn;
            self.tail = nn;

            // Push the new nodes
            for sz in vec.into_iter().rev() {
                self.allocate_first_fit(sz);
            }
        }
    }

    pub unsafe fn print_status(&self, fileptr: *mut libc::FILE) {
        let mut nd = self.head;
        let mut first = true;

        // Do a first-round no-space print because the grading script doesn't
        // cunting allow for terminal whitespacing
        unsafe {
            let node = &*nd;
            let nb = node.num_bytes();
            let alloc_stat: char = if node.in_use { 'A' } else { 'F' };
            let output = format!("{}{}", nb, alloc_stat).to_string();
            let c_output = std::ffi::CString::new(output).unwrap().into_raw();
            libc::fprintf(fileptr, c_output);
            // Need to free c_output by converting to owned or we leak memory
            let _ = std::ffi::CString::from_raw(c_output);
            nd = node.next;
        }

        while !nd.is_null() {
            unsafe {
                let node = &*nd;
                let nb = node.num_bytes();
                let alloc_stat: char = if node.in_use { 'A' } else { 'F' };
                let output = format!(" {}{}", nb, alloc_stat).to_string();
                let c_output = std::ffi::CString::new(output).unwrap().into_raw();
                libc::fprintf(fileptr, c_output);
                // Need to free c_output by converting to owned or we leak memory
                let _ = std::ffi::CString::from_raw(c_output);
                nd = node.next;
            }
        }
        let newline = std::ffi::CString::new("\n").unwrap().into_raw();
        libc::fprintf(fileptr, newline);
        let _ = std::ffi::CString::from_raw(newline);
    }
}

// Almost certainly not correct...but I'm not making multithread so I don't care
unsafe impl Send for FreeList {}
unsafe impl Sync for FreeList {}

unsafe impl Send for FreeNode {}
unsafe impl Sync for FreeNode {}

impl<'a> Iterator for FLIter<'a> {
    type Item = &'a FreeNode;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|node| {
            self.next = Some(unsafe { &*(node.next) });
            node
        })
    }
}

impl<'a> Iterator for FLRevIter<'a> {
    type Item = &'a FreeNode;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|node| {
            self.next = Some(unsafe { &*(node.prev) });
            node
        })
    }
}

impl<'a> Iterator for FLIterMut<'a> {
    type Item = &'a mut FreeNode;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node| {
            self.next = Some(unsafe { &mut *(node.next) });
            node
        })
    }
}

impl<'a> Iterator for FLRevIterMut<'a> {
    type Item = &'a mut FreeNode;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node| {
            self.next = Some(unsafe { &mut *(node.prev) });
            node
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_split_coalesce() {
        let lo_end = 0x4000 as *const u8;
        let hi_end = 0x8000 as *const u8;
        let mid_pt = 0x6000 as *const u8;
        let parent = FreeNode::new(lo_end, hi_end);

        // Simulate the action of an allocate call
        let (mut lo_node, mut hi_node) = parent.split_node(mid_pt);

        // Now free the hi_node to simulate the action of a free() call
        hi_node.in_use = false;

        let reconstructed = unsafe { FreeNode::coalesce_nodes(&mut lo_node, &hi_node) };
        assert_eq!(*reconstructed, parent);
    }

    #[test]
    fn test_basic_construction() {
        let buf = Box::<[u8; 1024]>::new([0; 1024]);
        let bufptr = &*buf as *const u8;
        let flist = FreeList::new(bufptr, 1024);
        let node = unsafe { &*flist.head };
        assert_eq!(
            *node,
            FreeNode {
                prev: std::ptr::null_mut(),
                next: std::ptr::null_mut(),
                lo_ptr: bufptr,
                hi_ptr: unsafe { bufptr.offset(1024) },
                in_use: false,
            }
        )
    }

    #[test]
    fn test_drop_split() {
        let buf = Box::<[u8; 1024]>::new([0; 1024]);
        let bufptr = &*buf as *const u8;
        let mut flist = FreeList::new(bufptr, 1024);

        let head = unsafe { &mut *flist.head };
        flist.allocate_from_node(head, 512);

        let node1 = unsafe { &*flist.head };
        let node2 = unsafe { &*flist.tail };

        //We can't guarantee the locations of next/prev in all cases, so check
        //only the components of the nodes that we know for sure must be a way.
        assert_eq!((*node1).prev, std::ptr::null_mut());
        assert_eq!((*node1).lo_ptr, bufptr);
        assert_eq!((*node1).hi_ptr, unsafe { bufptr.offset(512) });
        assert_eq!((*node1).in_use, false);

        assert_eq!((*node2).next, std::ptr::null_mut());
        assert_eq!((*node2).lo_ptr, unsafe { bufptr.offset(512) });
        assert_eq!((*node2).hi_ptr, unsafe { bufptr.offset(1024) });
        assert_eq!((*node2).in_use, true);
    }

    #[test]
    fn test_coalesce() {
        let buf = Box::<[u8; 1024]>::new([0; 1024]);
        let bufptr = &*buf as *const u8;
        let mut flist = FreeList::new(bufptr, 1024);

        let head = unsafe { &mut *flist.head };
        flist.allocate_from_node(head, 512);

        let node2 = unsafe { &*flist.tail };

        flist.free(node2.lo_ptr as *mut u8);

        let node = unsafe { &*flist.head };

        assert_eq!(
            *node,
            FreeNode {
                prev: std::ptr::null_mut(),
                next: std::ptr::null_mut(),
                lo_ptr: bufptr,
                hi_ptr: unsafe { bufptr.offset(1024) },
                in_use: false,
            }
        );
    }
}

mod freelist;

use libc::size_t;
use std::ffi::c_void;
use std::mem::MaybeUninit;
use std::sync::Mutex;

use freelist::{FreeList, FreeNode};

#[macro_use]
extern crate lazy_static;

const MB: usize = 1024 * 1024;
const HEAP_SZ: usize = 32 * MB;

static mut MALLOCBUF: [u8; HEAP_SZ] = [0; HEAP_SZ];
static mut FREELIST: Option<FreeList> = None;

// Magic function called by C code before calling malloc() for the first time.
// Much easier than dealing with it in Rust (and we'll just panic if it wasn't done)
#[no_mangle]
pub extern "C" fn my_mminit() {
    unsafe { FREELIST = Some(FreeList::new(&MALLOCBUF as *const u8, HEAP_SZ)) }
}

#[no_mangle]
pub extern "C" fn mm_best_fit_malloc(sz: size_t) -> *mut c_void {
    unsafe {
        let freelist = FREELIST.as_mut().unwrap();

        let retptr = freelist.allocate_best_fit(sz);

        match retptr {
            None => std::ptr::null_mut(),
            Some(x) => x as *mut c_void,
        }
    }
}

#[no_mangle]
pub extern "C" fn mm_first_fit_malloc(sz: size_t) -> *mut c_void {
    unsafe {
        let freelist = FREELIST.as_mut().unwrap();

        let retptr = freelist.allocate_first_fit(sz);

        match retptr {
            None => std::ptr::null_mut(),
            Some(x) => x as *mut c_void,
        }
    }
}

/*** Note: mm_malloc would go here, but it's enabled by preprocessor define magic
in the C code. Simplest way to deal with it is to let the C code define mm_malloc
with its preprocessor magic and let it call out to these rust functions */

#[no_mangle]
pub extern "C" fn mm_free(ptr: *mut c_void) -> () {
    unsafe {
        let freelist = FREELIST.as_mut().unwrap();
        freelist.free(ptr as *mut u8);
    }
}

#[no_mangle]
pub extern "C" fn mm_print_heap_status(ptr: *mut libc::FILE) {
    unsafe {
        let freelist = FREELIST.as_mut().unwrap();
        unsafe { freelist.print_status(ptr) };
    }
}

#[no_mangle]
pub extern "C" fn mm_defragment() {
    unsafe {
        let freelist = FREELIST.as_mut().unwrap();
        freelist.defragment();
    }
}

/* Here's what we need to implement:
void* mm_best_fit_malloc (size_t size);
void* mm_first_fit_malloc (size_t size);
void* mm_malloc (size_t size);
void mm_free (void* ptr);
void mm_print_heap_status (FILE*);
void mm_defragment()
*/

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
        let x = 5;
    }
}

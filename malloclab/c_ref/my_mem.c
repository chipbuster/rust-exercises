#include "my_mem.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * You CAN'T allocate a global array of memory nodes in static memory, for
 * example, you CAN'T do node[50], you HAVE to use pointers (linked lists). You
 * may need more than one linked list, although one is enough to keep/maintain
 * information about all memory ranges
 *
 * Feel free to use a different data structure, single/doubly linked lists,
 * trees, etc if you think they're better.
 *
 * Also, to make sure you do it correctly; all structures that you use for
 * bookkeeping (aka the list), you will use the system's malloc, not yours. Only
 * user's data will reside in the heap you are managing.
 */

// example: head of a list of nodes
MRNode *head;

// This is your "heap", a.k.a. the memory range you will manage. Imagine
// this is actually the entire RAM of a computer, and you are the OS
// managing memory requests from applications, but in a MUCH simpler
// and constrained scenario.
// Uou will reserve/allocate and manage memory from this block.
// If you run out of memory or can't allocate any more, you should "throw" a
// segmentation fault (your turn to get back at all segfaults that
// bothered you) and exit the program with an error (there's an example below)
// You won't need to extend/increase this heap.
// Note: In real OS, what happens when you do allocate more than what you have?
//      well, run "man sbrk" on your terminal, you'll learn how to use it in the
//      OS class
byte_ptr heap;

// just a toggle flag to initialize only once
static char mm_inited = 0;

void my_mminit();

// you shouldn't need to change this, but feel free to do so if you want
// it's just a switch to choose allocation policy during compile time
// and an init that only runs once
void *mm_malloc(size_t size) {
  if (!mm_inited) {
    my_mminit();
    mm_inited = 1;
  }

#ifdef BESTFIT
  return mm_best_fit_malloc(size);
#elif FIRSTFIT
  return mm_first_fit_malloc(size);
#else
  printf("memory system policy undefined\n");
  exit(1);
#endif
}
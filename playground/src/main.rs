fn main() {
    let x1 = Box::new(1);
    let x2 = Box::new(2);
    let r1 = &x1;
    let r2 = &x2;
    let mut r3 = &r1;
    r3 = &r2;

    println!("{}",r3);
}
